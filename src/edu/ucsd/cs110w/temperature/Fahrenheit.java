package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		float value = this.getValue();
		String string = ""+value +" F";
		return string;
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float value = this.getValue();
		value = value -32;
		value = value*5;
		value = value/9;
		Temperature output = new Celsius(value);
		return output;
	}
	@Override
	public Temperature toFahrenheit() {
		float value = this.getValue();
		Temperature output = new Fahrenheit(value);
		return output;
	}
	@Override
	public Temperature toKelvin() {
		float value = this.getValue();
		value = value -32;
		value = (float)(value/1.800);
		value = (float)(value + 273.15);
		Temperature output = new Kelvin(value);
		return output;
	}
}