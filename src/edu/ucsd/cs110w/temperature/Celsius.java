package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
	// TODO: Complete this method return "";
		float value = this.getValue();
		String string = ""+value +" C";
		return string;
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float value = this.getValue();
		Temperature output = new Celsius(value);
		return output;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float value = this.getValue();
		value = value*9;
		value = value/5;
		value = value+32;
		Temperature output = new Fahrenheit(value);
		return output;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		float value = this.getValue();
		value = (float)(value + 273.15);
		Temperature output = new Kelvin(value);
		return output;
	}
}