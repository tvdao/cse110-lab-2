package edu.ucsd.cs110w.temperature;

/**
 * 
 */

/**
 * TODO (cs110way): write class javadoc
 *
 * @author cs110way
 */
public class Kelvin extends Temperature {

	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
	// TODO: Complete this method
		float value = this.getValue();
		String string = ""+value +" K";
		return string;
	}
	@Override
	public Temperature toCelsius() {
	// TODO: Complete this method
		float value = this.getValue();
		value = (float)(value-273.15);
		Temperature output = new Celsius(value);
		return output;
	}
	@Override
	public Temperature toFahrenheit() {
	// TODO: Complete this method
		float value = this.getValue();
		value = (float)(value-273.15);
		value = (float)(value*1.8);
		value = value +32;
		Temperature output = new Fahrenheit(value);
		return output;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		float value = this.getValue();
		Temperature output = new Kelvin(value);
		return output;
	}
	
}
